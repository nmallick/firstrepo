﻿#Add-AzureAccount
#Select-AzureSubscription -SubscriptionId "c5e39c74-17c3-4a65-9183-d48dc0895bb0"
#$website = Get-AzureWebsite "emptyaspnetwebsite"
#$instance = $website.Instances[0].GetType()


Login-AzureRmAccount
$siteName = "emptyaspnetwebsite"
$rgName = "Default-Web-SouthCentralUS"


$websiteInstances = @()
$websiteInstances = Get-AzureRmResource -ResourceGroupName $rgName -ResourceType "Microsoft.Web/sites/instances" `
-ResourceName $siteName -ApiVersion 2015-11-01

#$subscriptionId = (Get-AzureRmContext).Subscription.SubscriptionId


"Name `t PID `t User_Name`t Handle_Count `t Private_Mem `t Virtual_mem `n"
"============================================================================================"
foreach($instance in $websiteInstances)
{
    $subscriptionId = $instance.SubscriptionId

    $instanceId = $instance.Name
    
    Write-Host("`n`tDetails for instance : {0}`n" -f $instanceId)  -BackgroundColor Green 
    

    $processList = Get-AzureRmResource -ResourceId `
    /subscriptions/$subscriptionId/resourceGroups/$rgName/providers/Microsoft.Web/sites/$siteName/instances/$instanceId/processes `
    -ApiVersion 2015-08-01
    foreach($process in $processList)
    {
        if(($process.Properties.Name -eq "w3wp") -or ($process.Properties.Name -eq "node"))
        {
            
            #$resourceId = "/subscriptions/$subscriptionId/resourceGroups/$rgName/providers/Microsoft.Web/sites/$siteName/instances/$instanceId/processes/" + $process.Properties.Id
            $resourceId = $process.ResourceId
            $processInfo = Get-AzureRmResource -ResourceId $resourceId -ApiVersion 2015-08-01
            if($processInfo.Properties.Is_scm_site -ne $true)
            {
            $processInfo.Properties.Name + "`t" + $processInfo.Properties.Id + "`t" + $processInfo.Properties.User_name + " " + $processInfo.Properties.Handle_count + " " + $processInfo.Properties.Total_cpu_time `
            + "`t" + $processInfo.Properties.Private_memory + "`t" + $processInfo.Properties.Virtual_memory            
            }
        }
    }
}


